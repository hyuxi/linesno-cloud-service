package com.alinesno.cloud.platform.hystrix.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

/**
 * 熔断器控制面板
 * 
 * @author WeiXiaoJin
 * @since 2019年8月6日 下午8:56:43
 *
 */
@EnableHystrix
@EnableTurbine
@EnableHystrixDashboard
@SpringBootApplication
public class ALinesnoApplication {
	public static void main(String[] args) {
		SpringApplication.run(ALinesnoApplication.class, args);
	}
}