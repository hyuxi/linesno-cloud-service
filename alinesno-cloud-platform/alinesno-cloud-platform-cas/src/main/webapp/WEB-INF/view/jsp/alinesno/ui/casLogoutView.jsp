<%@ page pageEncoding="UTF-8"%>
<jsp:directive.include file="includes/top.jsp" />
<body data-app-state="">
	<div id="root">
		<div>
			<div class="zJwEi">
				<div class="eaYrSC">
					<div class="jCprYf"></div>
					<div class="iMoAjM">
						<div class="vHoXo">
							<header class="dMTsYF">
								<jsp:directive.include file="includes/svg.jsp" />
							</header>
							<section role="main" class="ffnCIW">
								<div class="cMZWRp">
									<div>
										<div role="alert" class="bYGriA" style="box-shadow:none;">
											<h2><spring:message code="screen.logout.header" /></h2>
										    <p><spring:message code="screen.logout.success" /></p>
										    <p><spring:message code="screen.logout.security" /></p>
										</div>
									</div>
								</div>
							</section>
							<jsp:directive.include file="includes/bottom.jsp" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

