package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@Entity
@Table(name="sms_settings")
public class SmsSettingsEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 设置代码
     */
	@Column(name="settings_code")
	private String settingsCode;
    /**
     * 设置名称
     */
	@Column(name="settings_name")
	private String settingsName;
    /**
     * 设置名称
     */
	@Column(name="settings_value")
	private String settingsValue;
    /**
     * 设置过滤字段
     */
	@Column(name="settings_filter")
	private String settingsFilter;


	public String getSettingsCode() {
		return settingsCode;
	}

	public void setSettingsCode(String settingsCode) {
		this.settingsCode = settingsCode;
	}

	public String getSettingsName() {
		return settingsName;
	}

	public void setSettingsName(String settingsName) {
		this.settingsName = settingsName;
	}

	public String getSettingsValue() {
		return settingsValue;
	}

	public void setSettingsValue(String settingsValue) {
		this.settingsValue = settingsValue;
	}

	public String getSettingsFilter() {
		return settingsFilter;
	}

	public void setSettingsFilter(String settingsFilter) {
		this.settingsFilter = settingsFilter;
	}


	@Override
	public String toString() {
		return "SmsSettingsEntity{" +
			"settingsCode=" + settingsCode +
			", settingsName=" + settingsName +
			", settingsValue=" + settingsValue +
			", settingsFilter=" + settingsFilter +
			"}";
	}
}
