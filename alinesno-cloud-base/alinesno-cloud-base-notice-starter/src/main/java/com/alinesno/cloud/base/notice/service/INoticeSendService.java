package com.alinesno.cloud.base.notice.service;

import java.util.List;
import java.util.Map;

import com.alinesno.cloud.base.notice.dto.WechatMessageDto;
import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.entity.SmsSendEntity;
import com.alinesno.cloud.base.notice.euums.NoticeSendTypeEnum;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
public interface INoticeSendService {

	/**
	 * 发送纯文本短信
	 * 
	 * @param emailEntity
	 */
	public SmsSendEntity sendRealtimeSms(SmsSendEntity sms , NoticeSendTypeEnum noticeSendTypeEnum);

	/**
	 * 发送超文件短信(HTML)
	 * 
	 * @param emailEntity
	 * @return
	 * @throws Exception 
	 */
	public SmsSendEntity sendSms(SmsSendEntity sms , NoticeSendTypeEnum noticeSendTypeEnum) throws Exception;

	/**
	 * 发送模板短信
	 * 
	 * @param maps         属性值
	 * @param templateName 模板名称
	 * @param emailEntity  短信信息
	 * @return
	 */
	public SmsSendEntity sendTemplateSms(String templateName, SmsSendEntity sms , NoticeSendTypeEnum noticeSendTypeEnum);

	/**
	 * 批量发送Html短信
	 * 
	 * @param emails
	 * @return
	 */
	public SmsSendEntity sendBatchSms(List<String> sms, String subject, String htmlBody, NoticeSendTypeEnum noticeSendTypeEnum);

	/**
	 * 微信通知
	 * @param messageKey
	 * @param message
	 */
	public void sendWechatMessage(Map<String , Object> messageKey , WechatMessageDto message);

	/**
	 * 发送纯文本邮件
	 * 
	 * @param emailEntity
	 */
	public EmailSendEntity sendTextEmail(EmailSendEntity email, NoticeSendTypeEnum noticeSendTypeEnum);

	/**
	 * 发送超文件邮件(HTML)
	 * 
	 * @param emailEntity
	 * @return
	 * @throws Exception 
	 */
	public EmailSendEntity sendHtmlEmail(EmailSendEntity email, NoticeSendTypeEnum noticeSendTypeEnum) throws Exception;

	/**
	 * 发送模板邮件
	 * 
	 * @param maps         属性值
	 * @param templateName 模板名称
	 * @param emailEntity  邮件信息
	 * @return
	 */
	public EmailSendEntity sendTemplateEmail(Map<String, Object> maps, String templateName,
			EmailSendEntity email, NoticeSendTypeEnum noticeSendTypeEnum);

	/**
	 * 批量发送文本邮件
	 * 
	 * @param emails
	 * @return
	 */
	public EmailSendEntity sendBatchTextEmail(List<EmailSendEntity> emails, String subject,
			String htmlBody, NoticeSendTypeEnum noticeSendTypeEnum);

	/**
	 * 批量发送Html邮件
	 * 
	 * @param emails
	 * @return
	 */
	public EmailSendEntity sendBatchHtmlEmail(List<EmailSendEntity> emails, String subject,
			String htmlBody, NoticeSendTypeEnum noticeSendTypeEnum);

}
