package com.alinesno.cloud.base.notice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@Entity
@Table(name="email_template")
public class EmailTemplateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	@Column(name="template_content")
	private String templateContent;
	@Column(name="template_name")
	private String templateName;
	@Column(name="template_use_time")
	private Date templateUseTime;
	@Column(name="template_version")
	private Integer templateVersion;


	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public Date getTemplateUseTime() {
		return templateUseTime;
	}

	public void setTemplateUseTime(Date templateUseTime) {
		this.templateUseTime = templateUseTime;
	}

	public Integer getTemplateVersion() {
		return templateVersion;
	}

	public void setTemplateVersion(Integer templateVersion) {
		this.templateVersion = templateVersion;
	}


	@Override
	public String toString() {
		return "EmailTemplateEntity{" +
			"templateContent=" + templateContent +
			", templateName=" + templateName +
			", templateUseTime=" + templateUseTime +
			", templateVersion=" + templateVersion +
			"}";
	}
}
