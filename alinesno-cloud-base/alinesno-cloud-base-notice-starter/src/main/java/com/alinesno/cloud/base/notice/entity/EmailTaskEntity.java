package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@Entity
@Table(name="email_task")
public class EmailTaskEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;




	@Override
	public String toString() {
		return "EmailTaskEntity{" +
			"}";
	}
}
