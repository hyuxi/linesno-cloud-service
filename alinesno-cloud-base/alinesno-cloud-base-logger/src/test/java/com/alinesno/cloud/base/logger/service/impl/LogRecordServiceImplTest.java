package com.alinesno.cloud.base.logger.service.impl;

import org.databene.contiperf.PerfTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.logger.service.ILogRecordService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 调整记录
 * @author WeiXiaoJin
 * @since 2019年8月6日 下午8:32:09
 *
 */
public class LogRecordServiceImplTest extends JUnitBase {

	@Autowired
	private ILogRecordService logRecordService ; 

	@PerfTest(invocations = 10000 , threads = 100) // 并发测试
	@Test
	public void testHelloHystrix() {
		String name = null ; // "zhangnsan" ; 
		
		String hello = logRecordService.helloHystrix(name) ; 
		log.debug(hello);
	}

}
