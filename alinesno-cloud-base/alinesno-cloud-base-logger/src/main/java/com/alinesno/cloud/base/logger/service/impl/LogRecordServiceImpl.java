package com.alinesno.cloud.base.logger.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.base.logger.entity.LogRecordEntity;
import com.alinesno.cloud.base.logger.service.ILogRecordService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Service
public class LogRecordServiceImpl extends IBaseServiceImpl<LogRecordEntity, String> implements ILogRecordService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(LogRecordServiceImpl.class);

	/**
	 *  测试熔断器
	 *  由于Hystrix是一个容错框架，因此我们在使用的时候，要达到熔断的目的只需配置一些参数就可以了。但我们要达到真正的效果，就必须要了解这些参数。Circuit Breaker一共包括如下6个参数。 
	 *  1、circuitBreaker.enabled 
	 *  是否启用熔断器，默认是TURE。 
	 *  2、circuitBreaker.forceOpen 
	 *  熔断器强制打开，始终保持打开状态。默认值FLASE。 
	 *	3、circuitBreaker.forceClosed 
	 *	熔断器强制关闭，始终保持关闭状态。默认值FLASE。 
	 *	4、circuitBreaker.errorThresholdPercentage 
	 *	设定错误百分比，默认值50%，例如一段时间（10s）内有100个请求，其中有55个超时或者异常返回了，那么这段时间内的错误百分比是55%，大于了默认值50%，这种情况下触发熔断器-打开。 
	 *	5、circuitBreaker.requestVolumeThreshold 
	 *	默认值20.意思是至少有20个请求才进行errorThresholdPercentage错误百分比计算。比如一段时间（10s）内有19个请求全部失败了。错误百分比是100%，但熔断器不会打开，因为requestVolumeThreshold的值是20. 
	 */
	@HystrixCommand(commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000")
	}, fallbackMethod = "helloHystrixFallback")
	@Override
	public String helloHystrix(String name) {
	
		if(StringUtils.isBlank(name)) {
			 throw new RuntimeException("Exception to show hystrix enabled.");			
		}
		
		log.debug("name:{}" , name);
		return "hello , " + name;
	}
	
	public String helloHystrixFallback(String name) {
        return "Hystrix fallback , " + name;
    }

}
