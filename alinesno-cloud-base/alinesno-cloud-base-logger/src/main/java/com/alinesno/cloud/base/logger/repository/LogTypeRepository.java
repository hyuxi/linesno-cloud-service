package com.alinesno.cloud.base.logger.repository;

import com.alinesno.cloud.base.logger.entity.LogTypeEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
public interface LogTypeRepository extends IBaseJpaRepository<LogTypeEntity, String> {

}
