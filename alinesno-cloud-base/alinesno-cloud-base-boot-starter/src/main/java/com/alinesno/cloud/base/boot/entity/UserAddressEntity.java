package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="user_address")
public class UserAddressEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属者
     */
	private String owners;
    /**
     * 地址名称
     */
	@Column(name="address_name")
	private String addressName;
    /**
     * 是否使用
     */
	@Column(name="is_use")
	private String isUse;
    /**
     * 结束使用时间
     */
	@Column(name="use_end_time")
	private String useEndTime;
    /**
     * 开始使用时间
     */
	@Column(name="use_start_time")
	private String useStartTime;
    /**
     * 所属用户
     */
	@Column(name="user_id")
	private String userId;
    /**
     * 楼层
     */
	private String floor;
    /**
     * 房间号
     */
	@Column(name="room_num")
	private String roomNum;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getAddressName() {
		return addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public String getUseEndTime() {
		return useEndTime;
	}

	public void setUseEndTime(String useEndTime) {
		this.useEndTime = useEndTime;
	}

	public String getUseStartTime() {
		return useStartTime;
	}

	public void setUseStartTime(String useStartTime) {
		this.useStartTime = useStartTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}


	@Override
	public String toString() {
		return "UserAddressEntity{" +
			"owners=" + owners +
			", addressName=" + addressName +
			", isUse=" + isUse +
			", useEndTime=" + useEndTime +
			", useStartTime=" + useStartTime +
			", userId=" + userId +
			", floor=" + floor +
			", roomNum=" + roomNum +
			"}";
	}
}
