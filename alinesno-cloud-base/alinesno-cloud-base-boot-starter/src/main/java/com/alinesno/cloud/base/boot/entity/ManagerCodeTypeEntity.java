package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-02-07 21:16:11
 */
@Proxy(lazy = false)
@Entity
@Table(name="manager_code_type")
public class ManagerCodeTypeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 代码类型名称
     */
	@Column(name="code_type_name")
	private String codeTypeName;
    /**
     * 代码类型值
     */
	@Column(name="code_type_value")
	private String codeTypeValue;


	public String getCodeTypeName() {
		return codeTypeName;
	}

	public void setCodeTypeName(String codeTypeName) {
		this.codeTypeName = codeTypeName;
	}

	public String getCodeTypeValue() {
		return codeTypeValue;
	}

	public void setCodeTypeValue(String codeTypeValue) {
		this.codeTypeValue = codeTypeValue;
	}


	@Override
	public String toString() {
		return "ManagerCodeTypeEntity{" +
			"codeTypeName=" + codeTypeName +
			", codeTypeValue=" + codeTypeValue +
			"}";
	}
}
