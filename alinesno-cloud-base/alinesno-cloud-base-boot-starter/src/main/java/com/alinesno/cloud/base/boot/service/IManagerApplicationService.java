package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IManagerApplicationService extends IBaseService< ManagerApplicationEntity, String> {

	/**
	 * 查询用户应用
	 * @param accountId
	 * @return
	 */
	List<ManagerApplicationEntity> findAllByAccountId(String accountId);
	
	/**
	 * 删除应用同步删除菜单资源
	 * @param ids
	 * @return
	 */
	int deleteByApplicationId(String ids);

}
