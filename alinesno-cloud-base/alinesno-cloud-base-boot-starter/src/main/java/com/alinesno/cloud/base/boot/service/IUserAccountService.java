package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.UserAccountEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p> 基础账户表 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IUserAccountService extends IBaseService< UserAccountEntity, String> {

	/**
	 * 注册用户账号
	 * @param phone 手机号
	 * @param password 密码
	 * @param phoneCode 电话
	 * @return
	 */
	boolean registUser(String phone, String password, String phoneCode);

}
