package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRecordEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 22:18:49
 */
@NoRepositoryBean
public interface IManagerAccountRecordService extends IBaseService< ManagerAccountRecordEntity, String> {

}
