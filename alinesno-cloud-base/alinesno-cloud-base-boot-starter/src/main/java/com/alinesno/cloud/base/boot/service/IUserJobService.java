package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.UserJobEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IUserJobService extends IBaseService< UserJobEntity, String> {

}
