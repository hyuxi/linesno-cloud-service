package com.alinesno.cloud.base.boot.entity;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;
import org.hibernate.annotations.Proxy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.Date;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Proxy(lazy = false)
@Entity
@Table(name="content_posts")
public class ContentPostsEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 文章作者
     */
	@Column(name="post_author")
	private String postAuthor;
    /**
     * 文章日期
     */
	@Column(name="post_date")
	private Date postDate;
    /**
     * 文章内容 
     */
	@Lob
	@Column(name="post_content")
	private String postContent;
    /**
     * 文章标题
     */
	@Column(name="post_title")
	private String postTitle;
    /**
     * 当前状态(1草稿/2已发布)
     */
	@Column(name="post_status")
	private Integer postStatus;
    /**
     * 文章访问密码
     */
	@Column(name="post_password")
	private String postPassword;
    /**
     * 文章名称
     */
	@Column(name="post_name")
	private String postName;
    /**
     * 文章最后修改时间 
     */
	@Column(name="post_modifield")
	private Date postModifield;
    /**
     * 文章类型
     */
	@Column(name="post_type")
	private String postType;
    /**
     * 文章分类
     */
	@Column(name="post_mime_type")
	private String postMimeType;
    /**
     * 评论次数
     */
	@Column(name="comment_count")
	private Integer commentCount;
    /**
     * 转发
     */
	@Column(name="to_ping")
	private String toPing;


	public String getPostAuthor() {
		return postAuthor;
	}

	public void setPostAuthor(String postAuthor) {
		this.postAuthor = postAuthor;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public String getPostContent() {
		return postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public Integer getPostStatus() {
		return postStatus;
	}

	public void setPostStatus(Integer postStatus) {
		this.postStatus = postStatus;
	}

	public String getPostPassword() {
		return postPassword;
	}

	public void setPostPassword(String postPassword) {
		this.postPassword = postPassword;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public Date getPostModifield() {
		return postModifield;
	}

	public void setPostModifield(Date postModifield) {
		this.postModifield = postModifield;
	}

	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public String getPostMimeType() {
		return postMimeType;
	}

	public void setPostMimeType(String postMimeType) {
		this.postMimeType = postMimeType;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	public String getToPing() {
		return toPing;
	}

	public void setToPing(String toPing) {
		this.toPing = toPing;
	}


	@Override
	public String toString() {
		return "ContentPostsEntity{" +
			"postAuthor=" + postAuthor +
			", postDate=" + postDate +
			", postContent=" + postContent +
			", postTitle=" + postTitle +
			", postStatus=" + postStatus +
			", postPassword=" + postPassword +
			", postName=" + postName +
			", postModifield=" + postModifield +
			", postType=" + postType +
			", postMimeType=" + postMimeType +
			", commentCount=" + commentCount +
			", toPing=" + toPing +
			"}";
	}
}
