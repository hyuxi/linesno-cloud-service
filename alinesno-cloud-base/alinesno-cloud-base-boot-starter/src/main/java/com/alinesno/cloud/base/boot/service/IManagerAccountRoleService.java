package com.alinesno.cloud.base.boot.service;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRoleEntity;
import com.alinesno.cloud.common.facade.services.IBaseService;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 08:14:52
 */
@NoRepositoryBean
public interface IManagerAccountRoleService extends IBaseService< ManagerAccountRoleEntity, String> {

	List<ManagerAccountRoleEntity> findAllByAccountId(String accountId);

	void deleteByAccountId(String id);

}
