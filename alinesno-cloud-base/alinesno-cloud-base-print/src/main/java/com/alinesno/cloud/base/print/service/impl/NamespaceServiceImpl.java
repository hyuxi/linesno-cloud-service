package com.alinesno.cloud.base.print.service.impl;

import com.alinesno.cloud.base.print.entity.NamespaceEntity;
import com.alinesno.cloud.base.print.service.INamespaceService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@Service
public class NamespaceServiceImpl extends IBaseServiceImpl< NamespaceEntity, String> implements INamespaceService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(NamespaceServiceImpl.class);

}
