package com.alinesno.cloud.base.boot.repository;

import com.alinesno.cloud.base.boot.entity.ContentNoticeEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-04-04 14:20:07
 */
public interface ContentNoticeRepository extends IBaseJpaRepository<ContentNoticeEntity, String> {

}
