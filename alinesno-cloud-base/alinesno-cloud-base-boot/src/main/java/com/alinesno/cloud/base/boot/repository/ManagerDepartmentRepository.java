package com.alinesno.cloud.base.boot.repository;

import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.common.facade.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-03-24 13:24:58
 */
public interface ManagerDepartmentRepository extends IBaseJpaRepository<ManagerDepartmentEntity, String> {

}
