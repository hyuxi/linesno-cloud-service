package com.alinesno.cloud.base.boot.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRecordEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountRecordService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 22:18:49
 */
@Service
public class ManagerAccountRecordServiceImpl extends IBaseServiceImpl< ManagerAccountRecordEntity, String> implements IManagerAccountRecordService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ManagerAccountRecordServiceImpl.class);

}
