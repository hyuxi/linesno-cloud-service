package com.alinesno.cloud.base.boot.service.impl;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p> 参数配置表 服务实现类 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
@Service
public class ManagerSettingsServiceImpl extends IBaseServiceImpl< ManagerSettingsEntity, String> implements IManagerSettingsService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ManagerSettingsServiceImpl.class);

	@Override
	public ManagerSettingsEntity findOne(RestWrapper restWrapper) {
		log.debug("rest wrapper:{}" , restWrapper);
		
		return null ;
	}

	@Override
	public ManagerSettingsEntity queryKey(String key , String applicationId) {
		RestWrapper restWrapper = new RestWrapper() ; 
		restWrapper.eq("configKey", key) ; 
		if(StringUtils.isNotBlank(applicationId)) {
			restWrapper.eq("applicationId", applicationId) ; 
		}
	    Optional<ManagerSettingsEntity> dto = jpa.findOne(restWrapper.toSpecification()) ; //.findOne(restWrapper) ; 
	    return dto.isPresent()?dto.get():null ; 
	}

}
