package com.alinesno.cloud.base.boot.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.base.boot.enums.ResourceTypeEnmus;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.base.boot.service.IManagerDepartmentService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-03-24 13:24:58
 */
@Service
public class ManagerDepartmentServiceImpl extends IBaseServiceImpl< ManagerDepartmentEntity, String> implements IManagerDepartmentService {

	
	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ManagerDepartmentServiceImpl.class);

	@Autowired
	private IManagerApplicationService managerApplicationService ; 
	
	@Override
	public List<ManagerDepartmentEntity> findAllWithApplication(RestWrapper restWrapper) {
	
		List<ManagerDepartmentEntity> list = jpa.findAll(restWrapper.toSpecification()) ; 
		List<ManagerApplicationEntity> apps = managerApplicationService.findAll(restWrapper.toSpecification()) ; 
		
		for(ManagerApplicationEntity app : apps) {
			ManagerDepartmentEntity d = new ManagerDepartmentEntity() ; 
			d.setPid(ResourceTypeEnmus.PLATFORM_RESOURCE_PARENT.value);
			d.setId(app.getId());
			d.setFullName(app.getApplicationName());
			
			for(ManagerDepartmentEntity b : list) {
				if(app.getId().equals(b.getApplicationId()) && ResourceTypeEnmus.PLATFORM_RESOURCE_PARENT.value.equals(b.getPid())) {
					b.setPid(app.getId());
				}
			}
			list.add(d) ; 
		}
		
		return list;
	}
	
	@Cacheable(value = "ManagerDepartmentEntity") 
	@Override
	public Optional<ManagerDepartmentEntity> findById(String id) {
		log.debug("ManagerDepartmentEntity.findById :{}" , id);
		return super.findById(id);
	}


}
