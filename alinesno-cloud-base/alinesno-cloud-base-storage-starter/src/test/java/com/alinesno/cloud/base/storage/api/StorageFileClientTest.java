package com.alinesno.cloud.base.storage.api;

import com.alinesno.cloud.base.storage.entity.StorageFileEntity;
import com.alinesno.cloud.base.storage.service.IStorageFileService;
import org.apache.http.client.ClientProtocolException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StorageFileClientTest {

	private static final Logger log = LoggerFactory.getLogger(StorageFileClientTest.class) ; 
	
	@Autowired
	private IStorageFileService storageFileClient ; 

	@Test
	public void testUploadDataStringString() throws ClientProtocolException, IOException {
		log.debug("storage file client:{}" , storageFileClient);
		
		String localFile = "/Users/luodong/Desktop/demo_05.jpg" ; 
		StorageFileEntity dto = storageFileClient.uploadData(localFile) ; 
		log.debug("StorageFileEntity:{}" , dto);
	}

	@Test
	public void testUploadDataString() {
		fail("Not yet implemented");
	}

	@Test
	public void testDownloadData() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteData() {
		fail("Not yet implemented");
	}

}
