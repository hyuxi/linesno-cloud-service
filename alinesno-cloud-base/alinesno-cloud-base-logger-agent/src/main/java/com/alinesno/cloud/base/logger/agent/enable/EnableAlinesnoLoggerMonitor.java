package com.alinesno.cloud.base.logger.agent.enable;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * alinesno-cloud的web引导服务,包含有前端，登陆等服务组件
 * 
 * @author LuoAnDong
 * @sine 2019年4月5日 上午11:32:55
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({LoggerConfigurationSelector.class})
public @interface EnableAlinesnoLoggerMonitor {

	// //扫描feign包下的，变成接口可调用包

	/**
	 * If true, the ServiceRegistry will automatically register the local server.
	 */
	boolean autoRegister() default true;
}
