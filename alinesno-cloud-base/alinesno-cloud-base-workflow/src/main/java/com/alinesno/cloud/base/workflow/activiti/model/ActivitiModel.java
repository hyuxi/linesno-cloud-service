package com.alinesno.cloud.base.workflow.activiti.model;

/**
 * 工作流模型实体
 * @author LuoAnDong
 * @since 2019年9月1日 上午11:09:21
 */
public class ActivitiModel {

	private String description ;  // 模型描述
	private String name ;  // 模型名称
	private String key ;  // 模型主键 
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

}
