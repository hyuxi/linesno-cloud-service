package com.alinesno.cloud.base.workflow.controller;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 模型管理 
 * @author LuoAnDong
 * @since 2019年9月1日 下午1:49:24
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("/base/workflow/module")
public class ModuleController extends BaseController {

}
