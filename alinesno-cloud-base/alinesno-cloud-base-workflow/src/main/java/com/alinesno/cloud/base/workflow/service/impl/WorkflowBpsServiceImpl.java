package com.alinesno.cloud.base.workflow.service.impl;

import com.alinesno.cloud.base.workflow.activiti.WorkFlowService;
import com.alinesno.cloud.base.workflow.activiti.model.ActivitiModel;
import com.alinesno.cloud.base.workflow.bean.WorkItemBean;
import com.alinesno.cloud.base.workflow.entity.WorkflowBpsEntity;
import com.alinesno.cloud.base.workflow.repository.WorkflowBpsRepository;
import com.alinesno.cloud.base.workflow.service.IWorkflowBpsService;
import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;

import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * <p>  服务实现类 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-09-02 08:11:33
 */
@Service
public class WorkflowBpsServiceImpl extends IBaseServiceImpl<WorkflowBpsEntity, String> implements IWorkflowBpsService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(WorkflowBpsServiceImpl.class);

	@Autowired
	private WorkflowBpsRepository workflowBpsRepository ; 
	
	@Autowired
	private WorkFlowService workFlowSerivce ; 

	@Override
	public Page<Map<String, Object>> findActivitiInstance(Pageable pageable) {
		return workflowBpsRepository.findActivitiInstance(pageable);
	}

	@Override
	public String deploy(String id) {
		return workFlowSerivce.deploy(id) ; 
	}

	@Override
	public void deleteModel(String id) {
		workFlowSerivce.deleteModel(id);
	}

	@Override
	public String updateState(String state, String procDefId) {
		return workFlowSerivce.updateState(state, procDefId);
	}

	@Override
	public Model convertToModel(String procDefId) throws UnsupportedEncodingException, XMLStreamException {
		log.error("方法待实现.");
		return null;
	}

	@Override
	public InputStream resourceRead(String procDefId, String proInsId, String resType) throws Exception {
		return workFlowSerivce.resourceRead(procDefId, proInsId, resType) ; 
	}

	@Override
	public void deleteDeployment(String deploymentId) {
		workFlowSerivce.deleteDeployment(deploymentId);
	}

	@Override
	public String startProcess(String id, Model m, String defKey, String title, Map<String, Object> var, String username) {
		log.error("方法待实现.");
		return null ; 
	}

	@Override
	public void callBackTask(String taskId) {
		workFlowSerivce.callBackTask(taskId);
	}

	@Override
	public void backProcess(String taskId, String activityId, Map<String, Object> variables) throws Exception {
		workFlowSerivce.backProcess(taskId, activityId, variables); 
	}

	@Override
	public void callBackProcess(String taskId, String activityId) throws Exception {
		workFlowSerivce.callBackProcess(taskId, activityId);
	}

	@Override
	public void endProcess(String taskId) throws Exception {
		workFlowSerivce.endProcess(taskId);
	}

	@Override
	public List<WorkItemBean> findBackAvtivity(String taskId) throws Exception {
		List<ActivityImpl> list = workFlowSerivce.findBackAvtivity(taskId);
		log.debug("list:{}" , list);
		
		log.error("方法待优化返回.");
		return null ; 
	}

	@Override
	public void transferAssignee(String taskId, String userCode) {
		workFlowSerivce.transferAssignee(taskId, userCode);
	}

	@Override
	public String createModel(WorkItemBean model) throws UnsupportedEncodingException {
		ActivitiModel m = new ActivitiModel() ; 
		
		BeanUtils.copyProperties(model, m);
		
		return workFlowSerivce.createModel(m);
	}
}
