package com.alinesno.cloud.base.notice;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.alinesno.cloud.base.notice.auto.NoticeLocalConfigurationSelector;

/**
 * 实现基础服务
 * 
 * @author LuoAnDong
 * @sine 2019年4月5日 上午11:36:41
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({NoticeLocalConfigurationSelector.class})
public @interface EnableNoticeLocal {

	// //扫描feign包下的，变成接口可调用包

	/**
	 * If true, the ServiceRegistry will automatically register the local server.
	 */
	boolean autoRegister() default true;

	/**
	 * 默认不加载controller
	 * @return
	 */
	boolean autoController() default false; 
}