package com.alinesno.cloud.base.notice.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SmsRequestBean implements Serializable {

	private String phone ; //手机号码
	private String template ; //内容模板
	private String templateCode ; //模板代码
	private String outId ; //参数扩展
	private String signName ; //签名

	public String getTemplateCode() {
		return templateCode;
	}
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}
	public String getSignName() {
		return signName;
	}
	public void setSignName(String signName) {
		this.signName = signName;
	}
	public String getOutId() {
		return outId;
	}
	public void setOutId(String outId) {
		this.outId = outId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}

}