package com.alinesno.cloud.base.notice.wechat;

import java.util.Map;

import com.alinesno.cloud.base.notice.dto.WechatMessageDto;

import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

/**
 * 微信通知服务 
 * @author LuoAnDong
 * @since 2019年10月1日 下午9:55:21
 */
public interface IWechatNoticeService {

	/**
	 * 微信通知
	 * @param map
	 * @return
	 */
	public WxMpTemplateMessage sendMessage(Map<String , Object> map , WechatMessageDto m) ;  
	
}