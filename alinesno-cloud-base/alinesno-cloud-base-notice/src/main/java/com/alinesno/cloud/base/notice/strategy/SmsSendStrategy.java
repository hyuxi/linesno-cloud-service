package com.alinesno.cloud.base.notice.strategy;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.alinesno.cloud.base.notice.entity.SmsSendEntity;

public interface SmsSendStrategy {


	/**
	 * 发送纯文本短信
	 * @param emailEntity
	 * @throws Exception 
	 */
	public ResponseEntity<SmsSendEntity> sendRealtimeSms(SmsSendEntity sms) throws Exception ; 
	
	/**
	 * 发送超文件短信(HTML)
	 * @param emailEntity
	 * @return
	 * @throws Exception 
	 */
	public ResponseEntity<SmsSendEntity> sendSms(SmsSendEntity sms) throws Exception ; 

	/**
	 * 发送模板短信
	 * @param maps 属性值 
	 * @param templateName 模板名称
	 * @param emailEntity 短信信息
	 * @return
	 */
	public ResponseEntity<SmsSendEntity> sendTemplateSms(String templateName , SmsSendEntity sms) ; 
	
	/**
	 * 批量发送Html短信
	 * @param emails
	 * @return
	 */
	public ResponseEntity<SmsSendEntity> sendBatchSms(List<String> sms, String subject , String htmlBody) ; 

}
