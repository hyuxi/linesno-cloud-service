package com.alinesno.cloud.base.boot.web.module.content;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.base.boot.service.IContentPostTypeService;
import com.alinesno.cloud.base.boot.web.module.organization.AccountController;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.core.response.ResponseBean;
import com.alinesno.cloud.common.core.response.ResponseGenerator;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;

/**
 * 内容管理
 * @author LuoAnDong
 * @since 2019年4月4日 下午1:23:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/contentType")
public class ContentPostTypeController  extends FeignMethodController<ContentPostTypeEntity , IContentPostTypeService> {

	private static final Logger log = LoggerFactory.getLogger(AccountController.class) ; 

	@Reference
	private IContentPostTypeService iContentPostTypeService ; 

	/**
	 * 保存
	 * @param model
	 * @param request
	 * @param managerApplicationDto
	 * @return
	 */
	@FormToken(remove=true)
	@Override
	public ResponseBean update(Model model , HttpServletRequest request, ContentPostTypeEntity managerCodeTypeDto) {
		
		ContentPostTypeEntity oldBean = iContentPostTypeService.getOne(managerCodeTypeDto.getId()) ; 
		BeanUtil.copyProperties(managerCodeTypeDto, oldBean , CopyOptions.create().setIgnoreNullValue(true));
		
		managerCodeTypeDto = iContentPostTypeService.save(oldBean) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, iContentPostTypeService , page) ;
    }


	@Override
	public IContentPostTypeService getFeign() {
		return iContentPostTypeService ;
	}

	

//	@TranslateCode("[{hasStatus:has_status}]")
//	@ResponseBody
//	@RequestMapping("/typeDatatables")
//    public DatatablesPageBean typeDatatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
//		return this.toTypePage(model, feign , page) ;
//    }
//
//	private DatatablesPageBean toTypePage(Model model, IContentPostTypeService feign , DatatablesPageBean page) {
//		
//		// ----------------------- 兼容bootstrap table_start ---------------
//		if(page.isBootstrapTable()) {
//			page.setStart(page.getPageNum()-1);
//			page.setLength(page.getPageSize());
//		}
//		// ----------------------- 兼容bootstrap table_end ---------------
//		
//		RestWrapper restWrapper = new RestWrapper() ; 
//		restWrapper.setPageNumber(page.getStart());
//		restWrapper.setPageSize(page.getLength());
//		
//		restWrapper.builderCondition(page.getCondition()) ; 
//
//		Page<ContentPostTypeEntity> pR = feign.findAllByWrapperAndPageable(restWrapper) ; 
//		
//		if(page.isBootstrapTable()) {
//			// ----------------------- 兼容bootstrap table_start ---------------
//			page.setRows(pR.getContent());
//			page.setTotal((int) pR.getTotalElements());
//			page.setCode(HttpStatus.OK.value()); 
//			// ----------------------- 兼容bootstrap table_end ---------------
//		} else {
//			page.setData(pR.getContent());
//			page.setDraw(page.getDraw());
//			page.setRecordsFiltered((int) pR.getTotalElements());
//			page.setRecordsTotal((int) pR.getTotalElements());
//		}
//		
//		return page ;
//		
//	}
//	
	/**
	 * 删除
	 */
//	@ResponseBody
//	@PostMapping("/deleteType")
//    public ResponseBean deleteType(@RequestParam(value = "rowsId[]") String[] rowsId){
//		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
//		if(rowsId != null && rowsId.length > 0) {
//			feign.deleteByIds(rowsId); 
//		}
//		return ResponseGenerator.ok(null) ; 
//    }

	/**
	 * 删除
	 */
//	@ResponseBody
//	@PostMapping("/allType")
//    public List<ContentPostTypeEntity> allType(){
//		RestWrapper restWrapper = new RestWrapper() ; 
//		List<ContentPostTypeEntity> types = feign.findAllWithApplication(restWrapper) ; 
//		return types ; 
//    }
	
}
