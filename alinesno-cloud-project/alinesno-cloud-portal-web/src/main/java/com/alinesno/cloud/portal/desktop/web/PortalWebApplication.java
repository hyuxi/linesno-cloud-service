package com.alinesno.cloud.portal.desktop.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing ;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.alinesno.cloud.common.core.auto.EnableDruidMonitor;
import com.alinesno.cloud.common.web.enable.EnableLogin;

/**
 * 启动入口<br/>
 * @EnableSwagger2 //开启swagger2 
 * 
 * @author LuoAnDong 
 * @sine 2019-05-18 14:05:75
 */
@EnableJpaAuditing // jpa注解支持
@EnableLogin
@EnableDruidMonitor
@SpringBootApplication
public class PortalWebApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(PortalWebApplication.class, args);
	}
	
	@Configuration
	public static class SecurityPermitAllConfig extends WebSecurityConfigurerAdapter {
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http.authorizeRequests().anyRequest().permitAll()  
	            .and().csrf().disable()
	            .headers().frameOptions().disable(); 
	    }
	}
	
}
