package com.alinesno.cloud.portal.desktop.web.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathTypeEntity;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@NoRepositoryBean
public interface ILinkPathTypeService extends IBaseService<LinkPathTypeEntity, String> {

}
