package com.alinesno.cloud.portal.desktop.web.ucenter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ManagerSettingsEntity;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.login.aop.DataFilter;

/**
 * <p> 前端控制器 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 13:15:25
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/ucenter/settings")
public class ManagerSettingsController extends FeignMethodController<ManagerSettingsEntity, IManagerSettingsService> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ManagerSettingsController.class);

	@Reference
	private IManagerSettingsService managerSettingsService ;

	@DataFilter
	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
	
		List<ManagerSettingsEntity> list = managerSettingsService.tenantFindList(RestWrapper.create().eq("configKey", "sys.portal.style")) ; 
		System.out.println("list : " + JSONObject.toJSONString(list));
		
		
		return this.toPage(model, managerSettingsService , page) ;
    }

	@Override
	public IManagerSettingsService getFeign() {
		return managerSettingsService;
	}


}


























