package com.alinesno.cloud.portal.desktop.web.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 代理路径控制层
 * @author LuoAnDong
 * @since 2019年7月25日 下午2:38:56
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("public/proxy")
public class ProxyURLController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(ProxyURLController.class) ; 
	
	/**
	 * 代理路径 
	 * @return
	 */
	@RequestMapping("url")
	public String proxyUrl(String targetUrl) {
		log.debug("代理路径:{}" , targetUrl);
		
		return this.redirect(targetUrl)  ; 
	}
	
}
