package com.alinesno.cloud.portal.desktop.web.bean;

/**
 * 菜单实体对象 
 * @author LuoAnDong
 * @since 2019年4月19日 上午10:58:37
 */
public class MenuItemBean {

	private String id ;  
	private String menuName ; 
	private String menuIcon ; 
	private String menuLink ; 
	private String nav ; // 是否导航菜单 
	private String menuSort ; //排序
	
	private String typeId ; 
	private String typeName ;
	
	public String getNav() {
		return nav;
	}
	public void setNav(String nav) {
		this.nav = nav;
	}
	public String getMenuSort() {
		return menuSort;
	}
	public void setMenuSort(String menuSort) {
		this.menuSort = menuSort;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuIcon() {
		return menuIcon;
	}
	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}
	public String getMenuLink() {
		return menuLink;
	}
	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	} 
	
}
