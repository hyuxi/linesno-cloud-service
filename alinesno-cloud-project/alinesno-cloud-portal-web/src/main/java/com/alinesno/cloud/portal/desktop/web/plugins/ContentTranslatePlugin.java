package com.alinesno.cloud.portal.desktop.web.plugins;

import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.service.IContentPostTypeService;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;

/**
 * 文章内容转换
 * @author LuoAnDong
 * @since 2019年4月7日 下午10:04:58
 */
@Component
public class ContentTranslatePlugin implements TranslatePlugin {

	@Reference
	private IContentPostTypeService IContentPostTypeService ; 

	@Reference
	private IManagerAccountService IManagerAccountService ; 
	
	private String POSTMIMETYPE = "postMimeType" ; 
	private String POSTAUTHOR = "postAuthor" ; 
	
	@Override
	public void translate(JSONObject node, TranslateCode convertCode) {
		Object typeNode = node.get(POSTMIMETYPE) ; 
		
		String value = "" ; 
		if(typeNode != null) {
			log.debug("type node = {}" , typeNode.toString());
			if(StringUtils.isNotBlank(typeNode.toString())) {
				ContentPostTypeEntity dto = IContentPostTypeService.findEntityById(typeNode.toString()) ; 
				if(dto != null) {
					value = dto.getTypeName() ;
				}
			}
		}
		node.put(POSTMIMETYPE + LABEL_SUFFER, value) ; 
		
		Object authorTypeNode = node.get(POSTAUTHOR) ; 
		if(authorTypeNode != null) {
			log.debug("authorTypeNode = {}" , authorTypeNode.toString());
			
			ManagerAccountEntity dto = IManagerAccountService.findEntityById(authorTypeNode.toString()) ; 
			if(dto != null) {
				value = dto.getName() ;
			}
		}
		node.put(POSTAUTHOR + LABEL_SUFFER, value) ; 
	}

}
