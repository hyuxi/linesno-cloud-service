package com.alinesno.cloud.portal.desktop.web.bean;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
public class LinkPathBean {

    /**
     * 链接地址
     */
	private String linkPath;
	
    /**
     * 链接图标
     */
	private String linkLogo;
	
    /**
     * 链接描述
     */
	private String linkDesc;
	
    /**
     * 链接打开状态
     */
	private String linkTarget;
	
    /**
     * 链接描述
     */
	private String linkDesign;
	
    /**
     * 链接id
     */
	private String linkType;
	
    /**
     * 链接排序
     */
	private Integer linkSort;

	/**
     * 链接名称
     */
	private String linkName;

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}
	
	public String getLinkPath() {
		return linkPath;
	}

	public void setLinkPath(String linkPath) {
		this.linkPath = linkPath;
	}

	public String getLinkLogo() {
		return linkLogo;
	}

	public void setLinkLogo(String linkLogo) {
		this.linkLogo = linkLogo;
	}

	public String getLinkDesc() {
		return linkDesc;
	}

	public void setLinkDesc(String linkDesc) {
		this.linkDesc = linkDesc;
	}

	public String getLinkTarget() {
		return linkTarget;
	}

	public void setLinkTarget(String linkTarget) {
		this.linkTarget = linkTarget;
	}

	public String getLinkDesign() {
		return linkDesign;
	}

	public void setLinkDesign(String linkDesign) {
		this.linkDesign = linkDesign;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public Integer getLinkSort() {
		return linkSort;
	}

	public void setLinkSort(Integer linkSort) {
		this.linkSort = linkSort;
	}


	@Override
	public String toString() {
		return "LinkPathEntity{" +
			"linkPath=" + linkPath +
			", linkLogo=" + linkLogo +
			", linkDesc=" + linkDesc +
			", linkTarget=" + linkTarget +
			", linkDesign=" + linkDesign +
			", linkType=" + linkType +
			", linkSort=" + linkSort +
			"}";
	}
}
