package com.alinesno.cloud.portal.desktop.web.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.portal.desktop.web.entity.UserModuleEntity;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:10:44
 */
@NoRepositoryBean
public interface IUserModuleService extends IBaseService<UserModuleEntity, String> {

	boolean updateModule(String id, String rolesId);

	List<UserModuleEntity> findAllByUid(String id);

	/**
	 * 最近使用
	 * @param id
	 * @return
	 */
	List<UserModuleEntity> findOftenUse(String id);

}
