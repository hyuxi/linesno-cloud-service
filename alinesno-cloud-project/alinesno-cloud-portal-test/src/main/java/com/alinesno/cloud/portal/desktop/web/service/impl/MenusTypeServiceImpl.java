package com.alinesno.cloud.portal.desktop.web.service.impl;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.common.core.service.impl.IBaseServiceImpl;
import com.alinesno.cloud.portal.desktop.web.entity.MenusTypeEntity;
import com.alinesno.cloud.portal.desktop.web.service.IMenusTypeService;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Service
public class MenusTypeServiceImpl extends IBaseServiceImpl< MenusTypeEntity, String> implements IMenusTypeService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(MenusTypeServiceImpl.class);

}
