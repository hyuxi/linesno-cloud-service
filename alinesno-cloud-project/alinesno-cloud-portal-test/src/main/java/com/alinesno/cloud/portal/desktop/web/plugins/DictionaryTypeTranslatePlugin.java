package com.alinesno.cloud.portal.desktop.web.plugins;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.base.boot.entity.ManagerCodeTypeEntity;
import com.alinesno.cloud.base.boot.service.IManagerCodeTypeService;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;

/**
 * 部门内容转换
 * @author LuoAnDong
 * @since 2019年4月7日 下午10:04:58
 */
@Component
public class DictionaryTypeTranslatePlugin implements TranslatePlugin {

	@Reference
	private IManagerCodeTypeService managerCodeTypeFeigin ; 

	private String CODETYPEVALUE = "codeTypeValue" ; 
	
	@Override
	public void translate(JSONObject node, TranslateCode convertCode) {
		Object codeTypeValue = node.get(CODETYPEVALUE) ; 
		
		String value = "" ; 
		if(codeTypeValue != null) {
			log.debug("codeTypeValue node = {}" , codeTypeValue.toString());
			
			ManagerCodeTypeEntity dto = managerCodeTypeFeigin.findByCodeTypeValue(codeTypeValue.toString()) ; 
			if(dto != null) {
				value = dto.getCodeTypeName() ;
			}
		}
		node.put(CODETYPEVALUE + LABEL_SUFFER, value) ; 
	}

}
