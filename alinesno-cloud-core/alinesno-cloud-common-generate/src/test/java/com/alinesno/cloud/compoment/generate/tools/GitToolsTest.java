package com.alinesno.cloud.compoment.generate.tools;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.lib.Repository;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.compoment.generate.generator.tools.GitTools;

public class GitToolsTest{
	private final static Logger log = LoggerFactory.getLogger(GitTools.class);  
	private String username = "940417907@qq.com";
	private String password = "15916384523.+as";
	private String dirPath = "H:/repoTest/";
	private String clonePath = "H:/JGitTest0";
	private String httpsUrl = "https://gitee.com/jiahuahe/wd.git";

	/**
	 * 删除仓库
	 */
	@Test
	public void deleteGitRepository() {
		boolean deleteReuslt = GitTools.deleteGitRepository(dirPath);
		if(deleteReuslt) {
			log.info("删除成功");
		}else {
			log.info("删除失败");
		}
	}

	@Test
	public void createGitRepository() throws IOException {
		Repository newRepo = GitTools.createGitRepository(dirPath);
		log.error("新建或打开已有的仓库，分支名为：" + newRepo.getBranch());
		// 仓库状态
		Status repoStatus = GitTools.getGitStatus(dirPath);
		log.error("添加的文件：" + repoStatus.getAdded());
		log.error("修改的文件：" + repoStatus.getModified());
		log.error("改变的文件：" + repoStatus.getChanged());
		log.error("忽略的文件：" + repoStatus.getIgnoredNotInIndex());
		log.error("删除的文件：" + repoStatus.getRemoved());
		log.error("未提交文件：" + repoStatus.getUncommittedChanges());
		// 克隆
		if (GitTools.cloneGit(httpsUrl, clonePath, username, password)) {
			log.error("clone完成\n目录：" + clonePath);
		} else {
			log.error("clone失败");
		}

	}

	@Test
	public void checkoutGit() {
		if (GitTools.checkoutGit(clonePath,username,password)) {
			log.error("检出成功\n目录：" + clonePath);
		} else {
			log.error("检出失败");
		}
	}

	@Test
	public void createBranch() throws IOException {
		if (GitTools.createBranch("dev", clonePath)) {
			log.error("创建新分支成功\n分支名：dev");
		} else {
			log.error("创建新分支失败");
		}
		// 打开仓库
		Repository repo = GitTools.openGitRepository(dirPath);
		log.error("新打开仓库分支：" + repo.getBranch());
	}

	@Test
	public void commitAndPush() throws NoFilepatternException, IOException, GitAPIException {
		if (GitTools.commitAndPush(new File(clonePath), username, password, "jgit提交测试")) {
			log.error("提交成功");
		} else {
			log.error("提交失败");
		}

		log.info(httpsUrl.substring(httpsUrl.lastIndexOf("/") + 1).replace(".git", ""));
		
	}

}
