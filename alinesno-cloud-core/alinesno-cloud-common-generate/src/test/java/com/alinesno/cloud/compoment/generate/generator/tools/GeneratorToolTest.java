package com.alinesno.cloud.compoment.generate.generator.tools;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneratorToolTest {

	private final static Logger log = LoggerFactory.getLogger(GeneratorToolTest.class) ;

	@Test
	public void testToUpperCaseFirstOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testLineToHump2() {
		fail("Not yet implemented");
	}

	@Test
	public void testLineToHump() {
		fail("Not yet implemented");
	}

	@Test
	public void testHumpToLine() {
		fail("Not yet implemented");
	}

	@Test
	public void testHumpToLine2() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTypeName() {
		String type = "varchar(123)" ; 
		String name = GeneratorTool.getTypeName(type) ; 
		
		log.debug("name:{}" , name);
	}

	@Test
	public void testGetTypeLength() {
		String type = "varchar(123)" ; 
		type = "datetime" ; 
		
		String length = GeneratorTool.getTypeLength(type) ; 
		
		log.debug("length:{}" , length);
	}

}
