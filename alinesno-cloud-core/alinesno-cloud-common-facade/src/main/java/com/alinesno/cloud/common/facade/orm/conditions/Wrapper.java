package com.alinesno.cloud.common.facade.orm.conditions;

import java.util.Map;

import org.springframework.data.jpa.domain.Specification;

/**
 * spring data jpa 查询条件封装
 * @author LuoAnDong
 * @since 2018年11月20日 下午10:03:13
 */
public interface Wrapper<T> {

	/**
	 * 生成查询条件 
	 * @return
	 */
	Specification<T> handlerSpec(Map<String, Object> condition ) ; 

	/**
	 * 合并查询条件 
	 * @param spec1
	 * @param spec2
	 * @return
	 */
	Specification<T> mergeSpec(Specification<?> spec1 , Specification<?> spec2); 
	
}
