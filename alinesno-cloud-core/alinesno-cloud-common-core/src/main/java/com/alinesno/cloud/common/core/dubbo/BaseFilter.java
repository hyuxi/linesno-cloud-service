package com.alinesno.cloud.common.core.dubbo;

import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.RpcContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基础过滤器
 * @author LuoAnDong
 * @since 2019年10月6日 上午6:46:24
 */
public abstract class BaseFilter {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(BaseFilter.class) ;
	
	protected static final String APPLICATION_NAME = "applicationName";  // 应用名称
	protected static final String APPLICATION_ID = "applicationId";  // 应用id
	protected static final String TENANT_ID = "tenantId"; // 租户id
	protected static final String OPERATOR_ID = "operatorId" ; // 操作员id 
	
	protected static final String DUBBO_APPLICATION_NAME = "dubbo.application.name" ; 
	protected static final String DUBBO_APPLICATION_KEY_ID = "dubbo.application.key-id" ;
	protected static final String DUBBO_APPLICATION_TENANT_ID = "dubbo.application.tenant-id" ; 

	/**
	 * 设置参数
	 * @param invoker
	 * @param invocation
	 * @return 
	 */
	protected RpcContext attachment(Invoker<?> invoker,Invocation invocation) {
		return RpcContext.getContext()
				  .setInvoker(invoker)
				  .setInvocation(invocation);
	}
	
}
