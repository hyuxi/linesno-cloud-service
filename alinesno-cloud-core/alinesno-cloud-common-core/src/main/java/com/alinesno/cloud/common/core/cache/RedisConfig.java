package com.alinesno.cloud.common.core.cache;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;

/**
 * 配置自定义redis配置
 * @author LuoAnDong
 * @since 2019年7月7日 下午6:56:03
 */
@Configuration
public class RedisConfig {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(RedisConfig.class) ; 

	 //过期时间1天
    private Duration timeToLive = Duration.ofHours(12) ; 
    
    @Bean
    public RedisCacheManager cacheManager(RedisConnectionFactory connectionFactory) {
    	
        //默认1
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(this.timeToLive)
                .disableCachingNullValues();
        
        RedisCacheManager redisCacheManager = RedisCacheManager.builder(connectionFactory)
                .cacheDefaults(config)
                .transactionAware()
                .build();
        
        return redisCacheManager;
    } 
}