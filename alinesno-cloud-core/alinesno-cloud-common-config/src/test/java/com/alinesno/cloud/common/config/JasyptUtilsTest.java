package com.alinesno.cloud.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JasyptUtilsTest {

	private static final Logger log = LoggerFactory.getLogger(JasyptUtilsTest.class) ; 

	public static void main(String[] args) {
		
		String str = "AD跑腿镖局"; 
		String p = JasyptUtils.encryptPwd(str) ; 
		
		log.debug("加密结果，请复制:\nENC({})" , p);
	}
}
