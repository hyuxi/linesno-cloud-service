package com.alinesno.cloud.common.web.basic.auth;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)  
@SpringBootTest  
public class BasicAuthApplicationTests {

    @Autowired
    private WebApplicationContext context;

    @SuppressWarnings("unused")
	private MockMvc mvc;

    @Before
    public void setup() {  
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
//                .apply(springSecurity())  
                .build();
    }

    @Test
    public void basicAuthWithCustomPassword() throws Exception {
//        mvc.perform(
//                get("/").with(httpBasic("user", "password"))) 
//                        .andExpect(status().isOk());  
    }
}