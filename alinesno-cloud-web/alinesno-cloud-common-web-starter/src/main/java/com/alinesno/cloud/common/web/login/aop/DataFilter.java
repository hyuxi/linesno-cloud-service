package com.alinesno.cloud.common.web.login.aop;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.alinesno.cloud.common.web.login.constants.DataFilterRole;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataFilter {

	/**
	 * 默认不做拦截
	 * @return
	 */
	public DataFilterRole type() default DataFilterRole.OPERATOR ; 

}