package com.alinesno.cloud.common.web.login.config;

import org.apache.dubbo.config.annotation.Reference;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.base.boot.service.IManagerSettingsService;
import com.alinesno.cloud.common.web.login.dialect.PagesDialect;

/**
 * 方言配置
 * 
 * @author LuoAnDong
 * @since 2019年9月29日 下午18:21:35
 */
@Configuration
public class ShiroConfig {

	// >>>>>>>>>>>>>>>> 此处先初始化dubbo bean，然后转换成 springbean >>>>>>>>>>>>>>>>>>>>>>>>>>>
	// >>>>>>>>>>>>>>>> 处理shiro+dubbo springboot 为空的问题 >>>>>>>>>>>>>>>>>>>>>>>>>>>
	@Reference
	private IManagerSettingsService managerSettingsService;

	@Reference
	private IManagerAccountService managerAccountService;
	// >>>>>>>>>>>>>>>> 此处先初始化dubbo bean，然后转换成 springbean >>>>>>>>>>>>>>>>>>>>>>>>>>>

	@Bean
	public PagesDialect dataMaskingDialect() {
		return new PagesDialect();
	}

	/**
	 * 开启Shiro的注解(如@RequiresRoles,@RequiresPermissions),需借助SpringAOP扫描使用Shiro注解的类,并在必要时进行安全逻辑验证
	 * 配置以下两个bean(DefaultAdvisorAutoProxyCreator和AuthorizationAttributeSourceAdvisor)即可实现此功能
	 * 
	 * @return
	 */
	@Bean
	public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
		advisorAutoProxyCreator.setProxyTargetClass(true);
		return advisorAutoProxyCreator;
	}

	/**
	 * 开启aop注解支持
	 * 
	 * @param securityManager
	 * @return
	 */
	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
		return authorizationAttributeSourceAdvisor;
	}

}