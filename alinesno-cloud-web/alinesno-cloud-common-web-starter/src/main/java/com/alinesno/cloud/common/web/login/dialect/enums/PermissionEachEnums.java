package com.alinesno.cloud.common.web.login.dialect.enums;

/**
 * 菜单类型
 * @author LuoAnDong
 * @since 2019年1月14日 下午6:48:39
 */
public enum PermissionEachEnums {

	BUTTOM("0", "按钮"),
	SEARCH("1", "搜索框"),
	; 
	
	public String value ; //菜单值 
	public String menuName ; //菜单名称
	
	PermissionEachEnums(String value , String menuName){
		this.value = value ; 
		this.menuName = menuName ; 
	}
}
