package com.alinesno.cloud.common.web.login.dialect.processor;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring5.context.SpringContextUtils;
import org.thymeleaf.templatemode.TemplateMode;

import com.alinesno.cloud.base.boot.entity.ManagerCodeEntity;
import com.alinesno.cloud.base.boot.service.IManagerCodeService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 添加系统变量标签 
 * @author LuoAnDong
 * @since 2019年9月29日 下午19:07:58
 */
public final class SystemSettingsEleProcessor extends AbstractAttributeTagProcessor {
    
	 public static final int ATTR_PRECEDENCE = 300;
	public static final String ATTR_NAME = "params";
	
	private static final Logger log = LoggerFactory.getLogger(DictionaryTagProcessor.class) ; 


	public SystemSettingsEleProcessor(final String dialectPrefix) {
		super(
			TemplateMode.HTML, // This processor will apply only to HTML mode
			dialectPrefix,     // Prefix to be applied to name for matching
			null,              // No tag name: match any tag name
			false,             // No prefix to be applied to tag name
			ATTR_NAME,         // Name of the attribute that will be matched
			true,              // Apply dialect prefix to attribute name
			ATTR_PRECEDENCE,        // Precedence (inside dialect's precedence)
			true);             // Remove the matched attribute afterwards
	}


	protected void doProcess(
			final ITemplateContext context, final IProcessableElementTag tag,
			final AttributeName attributeName, final String attributeValue,
			final IElementTagStructureHandler structureHandler) {
		
		ApplicationContext ctx = SpringContextUtils.getApplicationContext(context);
		
		final IModelFactory modelFactory = context.getModelFactory();
		final IModel model = modelFactory.createModel();
		IManagerCodeService managerCodeService = ctx.getBean(IManagerCodeService.class) ; 
		
		final String type = tag.getAttributeValue("type"); // 类型
		final String value = tag.getAttributeValue("value"); // 值(默认值) 
		final String name = tag.getAttributeValue("name"); // 名称
		
		log.debug("type = {} , feigin = {}" , type , managerCodeService);
		
		StringBuffer sb = new StringBuffer() ; 
		if(StringUtils.isNotBlank(type)) {
			
			RestWrapper restWrapper = new RestWrapper() ; 
			restWrapper.eq("codeTypeValue", type)
					   .eq("hasStatus", "0") ; 
			List<ManagerCodeEntity> list = managerCodeService.findAll(restWrapper) ; 
			
			sb.append("<select class='form-control' id='"+name+"' name='"+name+"'>"); 
			
			for(ManagerCodeEntity m : list) {
				if(StringUtils.isNotBlank(value) && value.equals(m.getCodeValue())) {
					sb.append("<option value='"+m.getCodeValue()+"' selected>"+m.getCodeName()+"</option>") ; 
				} else {
					sb.append("<option value='"+m.getCodeValue()+"'>"+m.getCodeName()+"</option>") ; 
				}
			}
			
			sb.append("</select>") ; 
		}
		
		model.add(modelFactory.createText(sb));
		structureHandler.replaceWith(model, false);

	}
	
}
