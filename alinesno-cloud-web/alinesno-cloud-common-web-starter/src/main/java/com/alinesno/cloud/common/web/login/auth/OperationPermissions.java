package com.alinesno.cloud.common.web.login.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用户操作权限判断 , 仿shiro操作权限判断
 * @author LuoAnDong
 * @since 2019年10月3日 下午1:53:07
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OperationPermissions {

	/**
	 * 权限操作值
	 * @return
	 */
    String[] value();
   
    /**
     * 权限逻辑
     * @return
     */
    PermissionLogical logical() default PermissionLogical.AND; 

}

