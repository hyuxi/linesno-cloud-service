package com.alinesno.cloud.common.web.base.xss;

import java.util.Map;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Maps;

/**
 * xss配置 
 * @author WeiXiaoJin
 * @since 2019年7月14日 下午5:01:50
 */
@Configuration
public class XssConfig{
 
	/**
	 * xss过滤拦截器
	 */
	@Bean
	public FilterRegistrationBean<Filter> xssFilterRegistrationBean() {
		FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<Filter>();
		filterRegistrationBean.setFilter(new CrosXssFilter());
		filterRegistrationBean.setOrder(1);
		filterRegistrationBean.setEnabled(true);
		filterRegistrationBean.addUrlPatterns("/*");
		
		Map<String, String> initParameters = Maps.newHashMap();
		initParameters.put("isIncludeRichText", "true");
		
		filterRegistrationBean.setInitParameters(initParameters);
		return filterRegistrationBean;
	}
}
