package com.alinesno.cloud.common.web.login.form;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.alinesno.cloud.common.core.annotations.Phone;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
public class ManagerAccountVo {
	
	private String id ; 

	/**
	 * 所属者
	 */
	private String owners;
	/**
	 * 账户状态
	 */
	private String accountStatus;
	/**
	 * 最后登陆ip
	 */
	private String lastLoginIp;
	/**
	 * 最后登陆时间
	 */
	private String lastLoginTime;
	/**
	 * 登陆名称
	 */
	private String loginName;
	/**
	 * 登陆密码
	 */
	private String password;
	/**
	 * 加密字符
	 */
	private String salt;
	/**
	 * 用户信息id
	 */
	private String userId;
	/**
	 * 所属角色
	 */
	private String roleId;

	/**
	 * 用户名称.
	 */
	@NotBlank(message = "全称不能为空.")
	@Length(max = 32, min = 2, message = "全称长度不能小于6位且不能大于32位")
	private String name;
	/**
	 * 用户权限(9超级管理员/1租户权限/0用户权限)
	 */
	private String rolePower;

	/**
	 * 用户手机号
	 */
	@Phone
	@NotBlank(message = "手机号不能为空.")
	private String phone;

	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 性别
	 */
	@NotBlank(message = "性别不能为空.")
	@Length(max = 1, min = 1, message = "性别格式错误.")
	private String sex;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 所属部门
	 */
	private String department;

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRolePower() {
		return rolePower;
	}

	public void setRolePower(String rolePower) {
		this.rolePower = rolePower;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ManagerAccountEntity [owners=" + owners + ", accountStatus=" + accountStatus + ", lastLoginIp="
				+ lastLoginIp + ", lastLoginTime=" + lastLoginTime + ", loginName=" + loginName + ", password="
				+ password + ", salt=" + salt + ", userId=" + userId + ", roleId=" + roleId + ", name=" + name
				+ ", rolePower=" + rolePower + ", phone=" + phone + ", email=" + email + ", sex=" + sex
				+ ", department=" + department + "]";
	}

}
