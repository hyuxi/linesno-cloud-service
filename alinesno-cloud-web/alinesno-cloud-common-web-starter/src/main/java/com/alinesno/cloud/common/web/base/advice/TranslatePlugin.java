package com.alinesno.cloud.common.web.base.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonParser;

/**
 * 转换插件接口
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:24:08
 */
public interface TranslatePlugin {
	
	// 日志记录
	Logger log = LoggerFactory.getLogger(DatagridResponseBodyAdvice.class);
	String APPLICATION_ID = "applicationId" ; 
	String OPERATOR_ID = "operatorId" ; 
			
		
	Gson gson = new Gson() ; 
	JsonParser parser = new JsonParser();
	
	String LABEL_SUFFER = "Label" ; 

	/**
	 * 代码转换接口，用于页面返回代码转换
	 * @param node
	 * @param convertCode
	 */
	public void translate(JSONObject node , TranslateCode convertCode) ;  
	
}
