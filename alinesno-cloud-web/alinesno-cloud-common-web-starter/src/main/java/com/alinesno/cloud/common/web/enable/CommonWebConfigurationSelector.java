package com.alinesno.cloud.common.web.enable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class CommonWebConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>() ; 

		// common core 
		List<String> coreLoader = WebImportProvider.classLoader() ; 
		importBean.addAll(coreLoader) ; 
		
		return importBean.toArray(new String[] {}) ;
	}

	
}
