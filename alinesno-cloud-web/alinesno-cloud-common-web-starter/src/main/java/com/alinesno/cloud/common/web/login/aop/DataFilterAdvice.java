package com.alinesno.cloud.common.web.login.aop;

import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.login.constants.DataFilterRole;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;

/**
 * 数据权限过滤
 * 
 * @author LuoAnDong
 * @since 2019年9月15日 下午5:00:19
 */
@Component
@Aspect
public class DataFilterAdvice {

	// 日志记录
	private final static Logger log = LoggerFactory.getLogger(DataFilterAdvice.class);

	@Autowired
	private HttpServletRequest request; 

	// 拦截指定的方法,这里指只拦截TestService.getResultData这个方法
	@Pointcut("@annotation(com.alinesno.cloud.common.web.login.aop.DataFilter)")
	public void pointcut() {

	}

	// 执行方法前的拦截方法
	@SuppressWarnings("rawtypes")
	@Before("pointcut()")
	public void doBeforeMethod(JoinPoint joinPoint) throws NoSuchMethodException, SecurityException {
		// 获取目标方法的参数信息
		String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs(); // 请求接收的参数args 
        Class<?> targetClass = joinPoint.getTarget().getClass();
        Class[] parameterTypes = ((MethodSignature) joinPoint.getSignature()).getParameterTypes();
        Method methodClass = targetClass.getMethod(methodName, parameterTypes);
        DataFilter dataFilter = methodClass.getAnnotation(DataFilter.class) ; //.getAnnotations();
        
        DataFilterRole type = dataFilter.type() ;
        log.debug("data filter type :{} , request:{}" , type , request);
        
        for (Object argItem : args) {
        	log.debug("argItem:{}" , ToStringBuilder.reflectionToString(argItem));
        	if(argItem instanceof DatatablesPageBean) { // 分页参数
        		
        		DatatablesPageBean page = (DatatablesPageBean) argItem ; 
        		filterDataParams(page , type) ; 
        		
        	} else if(argItem instanceof RestWrapper) {
        		RestWrapper restWrapper = (RestWrapper) argItem ; 
        		
        		ManagerAccountEntity account = CurrentAccountSession.get(request) ; 
        		restWrapper.eq(type.getRoleFiled(), account.getId()) ; 
        		
        		argItem = restWrapper ; 
        	}
        }
        
	}

	/**
	 * 添加过滤参数
	 * @param page
	 * @param type 
	 */
	private void filterDataParams(DatatablesPageBean page, DataFilterRole type) {
	
		// 获取当前用户
		ManagerAccountEntity account = CurrentAccountSession.get(request) ; 
		Map<String, Object> paramMap = page.getCondition() ; 
		
		if(type == DataFilterRole.OPERATOR) { // 用户权限 
			
			paramMap.put(type.getRoleFiled(), account.getId()) ; 
			
		} else if(type == DataFilterRole.DEPARTMENT) {
			
			paramMap.put(type.getRoleFiled(), account.getDepartmentId()) ; 
		}
		
		page.setCondition(paramMap); 
	}

}
