package com.alinesno.cloud.common.web.login.auth;

/**
 * 没有操作权限异常
 * @author LuoAnDong
 * @since 2019年10月3日 下午1:55:59
 */
@SuppressWarnings("serial")
public class UnauthorizedOperationException extends RuntimeException {

    /**
     * Creates a new UnauthorizedException.
     */
    public UnauthorizedOperationException() {
        super();
    }

    /**
     * Constructs a new UnauthorizedException.
     *
     * @param message the reason for the exception
     */
    public UnauthorizedOperationException(String message) {
        super(message);
    }

    /**
     * Constructs a new UnauthorizedException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public UnauthorizedOperationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new UnauthorizedException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public UnauthorizedOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
