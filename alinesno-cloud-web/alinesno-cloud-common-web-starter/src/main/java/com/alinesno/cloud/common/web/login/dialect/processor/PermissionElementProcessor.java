package com.alinesno.cloud.common.web.login.dialect.processor;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring5.context.SpringContextUtils;
import org.thymeleaf.templatemode.TemplateMode;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;


/**
 * 列出所有按钮标签
 * @author LuoAnDong
 * @since 2019年10月3日 下午7:33:30
 */
public class PermissionElementProcessor extends AbstractElementTagProcessor {

	private static final Logger log = LoggerFactory.getLogger(PermissionElementProcessor.class) ; 
	
    private static final String ELEMENT_NAME = "permission";

    public PermissionElementProcessor(String dialectPrefix) {
        super(TemplateMode.HTML,dialectPrefix,ELEMENT_NAME,true,null,false,1000);
    }

    @Override
    protected void doProcess(ITemplateContext context ,
                             IProcessableElementTag element ,
                             IElementTagStructureHandler handler) {
    	
    	ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    	HttpServletRequest request = requestAttributes.getRequest();
        String requestURL = request.getRequestURL().toString() ; //.getContextPath();
        final IModelFactory modelFactory = context.getModelFactory();
		final IModel model = modelFactory.createModel();
        
        String pageId = request.getParameter("menuId") ;
        if(StringUtils.isBlank(pageId)) { // 不处理
        	return ; 
        }
        		
    	log.debug("requestURL:{} , MenuId:{}" , requestURL , pageId);
    
    	ApplicationContext ctx = SpringContextUtils.getApplicationContext(context);
    	ManagerAccountEntity account = ctx.getBean(CurrentAccountSession.class).get() ; 
    	IManagerResourceService managerResourceService = ctx.getBean(IManagerResourceService.class) ; 
    	
    	List<ManagerResourceEntity> list = managerResourceService.findResoucePermission(account , pageId) ; 
    
    	StringBuffer sb = new StringBuffer() ; 
    	for(ManagerResourceEntity e : list) {
    		log.debug("e :{}" , ToStringBuilder.reflectionToString(e));
    		sb.append("<a class=\"btn btn-success\" onclick=\""+ e.getPermissionScript() +"\">\n" + 
    				"	<i class=\""+e.getResourceIcon()+"\"></i> "+e.getResourceName()+"\n" + 
    				"</a>") ; 
    	}
    	
    	model.add(modelFactory.createText(sb));
		handler.replaceWith(model, false);
    }
}











