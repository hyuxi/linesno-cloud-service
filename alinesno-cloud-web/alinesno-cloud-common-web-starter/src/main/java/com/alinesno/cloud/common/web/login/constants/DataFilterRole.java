package com.alinesno.cloud.common.web.login.constants;

/**
 * 数据权限过滤角色
 * @author LuoAnDong
 * @since 2019年9月15日 下午5:02:33
 */
public enum DataFilterRole {

	APPLICATION("applicationId") ,  //所属应用 应用权限: 只能看到所属应用的权限【默认】
	TENANT("tenantId") , //所属租户 , 租户权限 
	OPERATOR("operatorId") , // 操作员  用户权限: 只能看到自己操作的数据
	FIELD("fieldId") , // 字段权限：部分字段权限无法加密或者不显示，或者大于某个值
	DEPARTMENT("department"),  // 部门权限: 只能看到自己所在部门的数据
	NOT_FILTER("notFilter") ; // 不做数据权限拦截

	private String roleFiled ; 
	
	DataFilterRole(String roleFiled){
		this.roleFiled = roleFiled ; 
	}

	public String getRoleFiled() {
		return roleFiled;
	}
	
}
