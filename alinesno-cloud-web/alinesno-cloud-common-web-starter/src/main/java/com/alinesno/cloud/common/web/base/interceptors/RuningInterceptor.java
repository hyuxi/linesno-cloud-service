package com.alinesno.cloud.common.web.base.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 判断是否超时
 * 
 * @author LuoAnDong
 * @since 2018年8月14日 上午8:14:09
 */
//@Component
public class RuningInterceptor implements HandlerInterceptor {

	private static final Logger log = LoggerFactory.getLogger(RuningInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.debug("request url = {}", request.getRequestURI());

		// if (pageSessionUtils.pageSession(request) != null) {
		//
		// // 判断用户是否被禁用
		// boolean isForbidden =
		// userService.isForbidden(pageSessionUtils.pageSession(request).getId());
		// logger.debug("isForbidden = {}" , isForbidden);
		// if (isForbidden) {
		// response.sendRedirect("/user_status");
		// return false ;
		// }
		//
		// }

		return true; // 只有返回true才会继续向下执行，返回false取消当前请求
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

}