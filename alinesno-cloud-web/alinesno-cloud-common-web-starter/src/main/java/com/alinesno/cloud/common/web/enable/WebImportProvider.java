package com.alinesno.cloud.common.web.enable;

import java.util.List;

import com.alinesno.cloud.common.core.auto.CoreImportProvider;
import com.alinesno.cloud.common.web.base.advice.DatagridResponseBodyAdvice;
import com.alinesno.cloud.common.web.base.advice.plugins.ApplicationPlugin;
import com.alinesno.cloud.common.web.base.advice.plugins.DirectoryPlugin;
import com.alinesno.cloud.common.web.base.advice.plugins.ManagerAccounntPlugin;
import com.alinesno.cloud.common.web.base.controller.PageJumpController;
import com.alinesno.cloud.common.web.base.controller.WebStorageController;
import com.alinesno.cloud.common.web.base.exceptions.WebExceptionAspect;
import com.alinesno.cloud.common.web.base.exceptions.WebExceptionHandler;
import com.alinesno.cloud.common.web.base.form.DateConverterConfig;
import com.alinesno.cloud.common.web.base.form.FormTokenAop;
import com.alinesno.cloud.common.web.base.utils.ManagerSettingsUtils;
import com.alinesno.cloud.common.web.base.utils.WebUploadUtils;
import com.alinesno.cloud.common.web.base.xss.XssConfig;
import com.alinesno.cloud.common.web.login.aop.AccountRecordAspect;
import com.alinesno.cloud.common.web.login.aop.DataFilterAdvice;
import com.alinesno.cloud.common.web.login.controller.CommonController;
import com.alinesno.cloud.common.web.login.controller.DashboardController;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;
import com.alinesno.cloud.common.web.login.session.RedisSessionConfig;

/**
  *   公共 web包提供接口
 * 
 * @author WeiXiaoJin
 * @since 2019年7月14日 下午5:10:48
 */
public class WebImportProvider {

	/**
	 * 提供接口对象
	 * 
	 * @return
	 */
	public static List<String> classLoader() {

		// common core
		List<String> coreLoader = CoreImportProvider.classLoader();
		coreLoader.addAll(coreLoader);

		// common web
		coreLoader.add(FormTokenAop.class.getName());
		coreLoader.add(WebUploadUtils.class.getName());
//		coreLoader.add(ShiroConfig.class.getName());
		coreLoader.add(WebStorageController.class.getName());
		coreLoader.add(PageJumpController.class.getName());
		 coreLoader.add(DateConverterConfig.class.getName());
		
		// 异常
		coreLoader.add(WebExceptionHandler.class.getName());
		coreLoader.add(WebExceptionAspect.class.getName());
		coreLoader.add(CurrentAccountSession.class.getName());

//		coreLoader.add(BeaEntityAdvice.class.getName());
		coreLoader.add(DatagridResponseBodyAdvice.class.getName());
		coreLoader.add(DataFilterAdvice.class.getName());
		coreLoader.add(DirectoryPlugin.class.getName());
		coreLoader.add(ManagerAccounntPlugin.class.getName());
		coreLoader.add(ApplicationPlugin.class.getName());
		coreLoader.add(AccountRecordAspect.class.getName());

		coreLoader.add(DashboardController.class.getName());
		coreLoader.add(CommonController.class.getName());
		coreLoader.add(RedisSessionConfig.class.getName());
		coreLoader.add(ManagerSettingsUtils.class.getName());

		// xss 过滤
		coreLoader.add(XssConfig.class.getName());

		return coreLoader;
	}

}