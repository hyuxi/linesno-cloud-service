package com.alinesno.cloud.common.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alinesno.cloud.common.job.annotation.EnableElasticJob;

/**
 * 启动入口
 *
 * @author WeiXiaoJin
 * @sine 2019-10-04 22:10:47
 */
@EnableElasticJob
@SpringBootApplication
public class JobApplication {
	public static void main(String[] args) {
		SpringApplication.run(JobApplication.class, args);
	}
}

