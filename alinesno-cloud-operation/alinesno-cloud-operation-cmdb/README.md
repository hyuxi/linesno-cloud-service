flyway.baseline-description对执行迁移时基准版本的描述.
flyway.baseline-on-migrate当迁移时发现目标schema非空，而且带有没有元数据的表时，是否自动执行基准迁移，默认false.
flyway.baseline-version开始执行基准迁移时对现有的schema的版本打标签，默认值为1.
flyway.check-location检查迁移脚本的位置是否存在，默认false.
flyway.clean-on-validation-error当发现校验错误时是否自动调用clean，默认false.
flyway.enabled是否开启flywary，默认true.
flyway.encoding设置迁移时的编码，默认UTF-8.
flyway.ignore-failed-future-migration当读取元数据表时是否忽略错误的迁移，默认false.
flyway.init-sqls当初始化好连接时要执行的SQL.
flyway.locations迁移脚本的位置，默认db/migration.
flyway.out-of-order是否允许无序的迁移，默认false.
flyway.password目标数据库的密码.
flyway.placeholder-prefix设置每个placeholder的前缀，默认${.
flyway.placeholder-replacementplaceholders是否要被替换，默认true.
flyway.placeholder-suffix设置每个placeholder的后缀，默认}.
flyway.placeholders.[placeholder name]设置placeholder的value
flyway.schemas设定需要flywary迁移的schema，大小写敏感，默认为连接默认的schema.
flyway.sql-migration-prefix迁移文件的前缀，默认为V.
flyway.sql-migration-separator迁移脚本的文件名分隔符，默认__
flyway.sql-migration-suffix迁移脚本的后缀，默认为.sql
flyway.tableflyway使用的元数据表名，默认为schema_version
flyway.target迁移时使用的目标版本，默认为latest version
flyway.url迁移时使用的JDBC URL，如果没有指定的话，将使用配置的主数据源
flyway.user迁移数据库的用户名
flyway.validate-on-migrate迁移时是否校验，默认为true.

功能列表：
- 仪盘表： 总体监控及信息管理
- 资产管理：所有硬件资产管理和记录，资源申请
- 主机管理：物理机管理，虚拟管理
- 数据管理：数据库配置、数据库管理
- 平台任务:自动部署、运行脚本
- 参数配置：预警参数、参数管理、机房管理
- 用户管理：权限分配、用户管理


资产类型包括：

服务器
存储设备
安全设备
网络设备
软件资产
服务器又可分为：

刀片服务器
PC服务器
小型机
大型机
其它
存储设备包括：

磁盘阵列
网络存储器
磁带库
磁带机
其它
安全设备包括：

防火墙
入侵检测设备
互联网网关
漏洞扫描设备
数字签名设备
上网行为管理设备
运维审计设备
加密机
其它
网络设备包括：

路由器
交换器
负载均衡
VPN
流量分析
其它
软件资产包括：

操作系统授权
大型软件授权
数据库授权
其它
其中，服务器是运维部门最关心的，也是CMDB中最主要、最方便进行自动化管理的资产。

服务器又可以包含下面的部件：

CPU
硬盘
内存
网卡
除此之外，我们还要考虑下面的一些内容：

机房
业务线
合同
管理员
审批员
资产标签
其它未尽事宜
大概对资产进行了分类之后，就要详细考虑各细分数据条目了。

共有数据条目：

有一些数据条目是所有资产都应该有的，比如：

资产名称
资产sn
所属业务线
设备状态
制造商
管理IP
所在机房
资产管理员
资产标签
合同
价格
购买日期
过保日期
批准人
批准日期
数据更新日期
备注
另外，不同类型的资产还有各自不同的数据条目，例如服务器：

服务器：

服务器类型
添加方式
宿主机
服务器型号
Raid类型
操作系统类型
发行版本
操作系统版本