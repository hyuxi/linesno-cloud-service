package com.alinesno.cloud.operation.cmdb.service;

import java.util.List;

import com.alinesno.cloud.operation.cmdb.entity.IntegralRecordEntity;

/**
 * 积分服务
 * @author LuoAnDong
 * @since 2018年10月20日 下午9:36:04
 */
public interface IntegralService {

	List<IntegralRecordEntity> findIntegralRecord(String userId);

}
