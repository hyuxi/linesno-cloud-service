package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.alinesno.cloud.operation.cmdb.entity.IntegralRecordEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface IntegralRecordRepository extends BaseJpaRepository<IntegralRecordEntity, String> {

	List<IntegralRecordEntity> findByUserIdOrderByAddTimeDesc(Pageable pageable, String userId);

}