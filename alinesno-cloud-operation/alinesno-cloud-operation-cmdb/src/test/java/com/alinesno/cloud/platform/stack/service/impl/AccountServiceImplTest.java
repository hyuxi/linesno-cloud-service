package com.alinesno.cloud.platform.stack.service.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.operation.cmdb.entity.ResourceEntity;
import com.alinesno.cloud.operation.cmdb.service.AccountService;
import com.alinesno.cloud.platform.stack.JUnitBase;

public class AccountServiceImplTest extends JUnitBase {

	@Autowired
	private AccountService accountService ; 
	
	@Test
	public void testFindResourceByUserId() {
		logger.debug("accountService = {}" , accountService);
		String accountId = "479922982433587200" ; 
		List<ResourceEntity> list = accountService.findResourceByUserId(accountId) ; 
		
		Assert.assertNotNull(list);
		Assert.assertTrue(list.size()>0);
		
		for(ResourceEntity l : list) {
			logger.debug("l = {}" , l);
		}
		
	}

}
