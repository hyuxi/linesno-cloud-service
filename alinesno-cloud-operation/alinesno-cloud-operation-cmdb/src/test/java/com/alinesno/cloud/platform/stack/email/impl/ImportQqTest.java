package com.alinesno.cloud.platform.stack.email.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import com.alinesno.cloud.operation.cmdb.common.util.EmojiFilter;
import com.alinesno.cloud.operation.cmdb.common.util.RunStringUtils;
import com.alinesno.cloud.operation.cmdb.entity.EmailEntity;
import com.alinesno.cloud.operation.cmdb.repository.EmailRepository;
import com.vdurmont.emoji.EmojiParser;

/**
 * 导入qq至数据库
 * 
 * @author LuoAnDong
 * @since 2018年8月29日 下午7:36:40
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ImportQqTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmailRepository emailRepository;

	@Test
	public void testImportEmail() throws FileNotFoundException {

		File file = ResourceUtils.getFile("/Users/luodong/GitResponsitory/switch_run_document/04_qq群文件");
		
		for(File f : file.listFiles()) {
			String html = RunStringUtils.readToString(f);

			Document doc = Jsoup.parse(html);
			Elements eles = doc.getElementsByTag("tr");
			int i = 1 ; 
			for (Element e : eles) {
				Elements tdEles = e.getElementsByTag("td") ; 
				
				String email = tdEles.get(4).text()+"@qq.com" ; 
				String name = RunStringUtils.replaceBlank(EmojiParser.removeAllEmojis(StringUtils.isBlank(RunStringUtils.replaceBlank(tdEles.get(3).text()))?tdEles.get(2).getElementsByTag("span").get(0).text():tdEles.get(3).text()))  ; 
				String newName = EmojiFilter.filterEmoji(StringUtils.isBlank(name)?"用户":name) ;
				String sex = tdEles.get(5).text() ;  
				
				logger.debug("index = {} , email = {} , name = {} , sex = {}" , (i++) , email , newName , sex);
				
				EmailEntity ee = new EmailEntity() ; 
				ee.setAddTime(new Date());
				ee.setEmail(email); 
				ee.setEmailOwner(newName);
				ee.setSex(sex.contains("男")?"1":"0");  
			
				List<EmailEntity> emailEntity = emailRepository.findByEmail(email) ; 
				if(emailEntity == null || emailEntity.size() == 0) {
					emailRepository.save(ee) ; 
					logger.debug("插入邮件{}成功" , email);
				}
				
			}
		}
	}

}
