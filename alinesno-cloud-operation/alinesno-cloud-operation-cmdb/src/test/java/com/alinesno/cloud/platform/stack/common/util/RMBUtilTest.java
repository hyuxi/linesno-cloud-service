package com.alinesno.cloud.platform.stack.common.util;

import org.junit.Test;

import com.alinesno.cloud.operation.cmdb.common.util.RMBUtil;

public class RMBUtilTest {

	@Test
	public void testIsNumber() {
		String a = "1.23" ; 
		String b = "3.12" ; 
		String c = "4.23" ; 
		boolean aIs = RMBUtil.isNumber(a) ; 
		boolean bIs = RMBUtil.isNumber(b) ; 
		boolean cIs = RMBUtil.isNumber(c) ; 
		
		System.out.println("aIs = " +aIs +", bIs = " +bIs + " , cIs = " +cIs);
	}
	
	/**
	 * RMB格式化 
	 * 
	 * @param value
	 * @return String
	 * @throws Exception
	 */
	@Test
	public void testCommaFormat() {
		String a = "1.23" ; 
		String b = "3" ; 
		String c = "4.1264" ; 
		
		try {
			String aIs = RMBUtil.CommaFormat(a) ; 
			String bIs = RMBUtil.CommaFormat(b) ; 
			String cIs = RMBUtil.CommaFormat(c) ; 
			
			System.out.println("aIs = " +aIs +", bIs = " +bIs + " , cIs = " +cIs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



}
