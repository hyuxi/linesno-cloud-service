## 工程介绍
### 概述
对外提供网关服务，类似于对外的统一接口，提供Http服务示例,针对于app或者前后端分离的业务场景。

### 业务架构 

[api] ----Restful----> [gateway] ----dubbo协议----> [dubboA、dubboB、dubboC、dubboD]  

[gate-proxy] <----restful--- [gate-admin]

### Http接口样例 

**请求URL：** 
- ` http://localhost:27008/api/user/register `

- URL 参数说明:
/api/{version}/{model}/{method}

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|version |是  |string |接口版本号   |
|model|是  |string | 所属模块    |
|method |否  |string | 所属接口  |

**请求方式：**
- POST  

**参数：** 

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|username |是  |string |用户名   |
|password |是  |string | 密码    |
|name     |否  |string | 昵称    |

**返回示例**

``` 
  {
    "code": 0,
    "message":"请求成功" , 
    "data": {
      "uid": "1",
      "username": "12154545",
      "name": "吴系挂",
      "groupid": 2 ,
      "reg_time": "1436864169",
      "last_login_time": "0",
    }
  }
```

 **返回参数说明** 

|参数名|类型|说明|
|:-----  |:-----|-----                           |
|code|int   |状态码|
|message |string |提示语|
|data|string ||


### Socket 接口样例 

前6位代表长度，7~13位代表代码，后面代表报文

报文格式定义:
前4位固定+8位长度+内容，如

```xml
0x8000000819<?xml version="1.0" encoding="UTF-8"?>
<message>
    <!-- 消息头部 -->
    <head>
        <!-- 所属业务代码 -->
        <business>BUSINESS_001</business>
        <!-- 租户 -->
        <tenant>TENTANT_001</tenant>
        <!-- 所属接口 -->
        <api>API_001</api>
        <!-- 交易代码 -->
        <transaction></transaction>
    </head>
    <!-- 消息内容 -->
    <body>
        <userId>00001</userId>
    </body>
</message>
```