package com.alinesno.cloud.gateway.dashboard.admin.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.facade.services.IBaseService;
import com.alinesno.cloud.gateway.dashboard.admin.entity.GateCollectEntity;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@NoRepositoryBean
public interface IGateCollectService extends IBaseService<GateCollectEntity, String> {

}
