package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_logger")
public class GateLoggerEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * api链接
     */
	private String url;
    /**
     *  请求方式
     */
	private String method;
    /**
     * 参数列表
     */
	private String param;
    /**
     * 请求参数备注
     */
	private String paramRemark;
    /**
     * 请求示例
     */
	private String requestExam;
    /**
     * 返回参数说明
     */
	private String responseParam;
    /**
     * 接口错误码列表
     */
	private String errorList;
    /**
     * 正确返回示例
     */
	private String trueExam;
    /**
     * 错误返回示例
     */
	private String falseExam;
    /**
     * 是否可用;0不可用；1可用;-1 删除
     */
	private Integer status;
    /**
     * 所属模块ID
     */
	private String moduleId;
    /**
     * 接口名
     */
	private String interfaceName;
	private String remark;
    /**
     * 错误码、错误码信息
     */
	private String errors;
	private String updateBy;
	private Date createTime;
    /**
     * 版本号
     */
	private String version;
    /**
     * 排序，越大越靠前
     */
	private Long sequence;
	private String header;
	private String fullUrl;
    /**
     * 监控类型，多选：
Network("网络异常",1),Include("包含指定字符串",2),NotInclude("不包含指定字符串",3),NotEqual("不等于指定字符串",4);	

     */
	private Integer monitorType;
    /**
     * 监控比较内容
     */
	private String monitorText;
	private String monitorEmails;
    /**
     * 是否是模板
     */
	private Boolean isTemplate;
	private String projectId;
    /**
     * 接口返回contentType
     */
	private String contentType;


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getParamRemark() {
		return paramRemark;
	}

	public void setParamRemark(String paramRemark) {
		this.paramRemark = paramRemark;
	}

	public String getRequestExam() {
		return requestExam;
	}

	public void setRequestExam(String requestExam) {
		this.requestExam = requestExam;
	}

	public String getResponseParam() {
		return responseParam;
	}

	public void setResponseParam(String responseParam) {
		this.responseParam = responseParam;
	}

	public String getErrorList() {
		return errorList;
	}

	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}

	public String getTrueExam() {
		return trueExam;
	}

	public void setTrueExam(String trueExam) {
		this.trueExam = trueExam;
	}

	public String getFalseExam() {
		return falseExam;
	}

	public void setFalseExam(String falseExam) {
		this.falseExam = falseExam;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFullUrl() {
		return fullUrl;
	}

	public void setFullUrl(String fullUrl) {
		this.fullUrl = fullUrl;
	}

	public Integer getMonitorType() {
		return monitorType;
	}

	public void setMonitorType(Integer monitorType) {
		this.monitorType = monitorType;
	}

	public String getMonitorText() {
		return monitorText;
	}

	public void setMonitorText(String monitorText) {
		this.monitorText = monitorText;
	}

	public String getMonitorEmails() {
		return monitorEmails;
	}

	public void setMonitorEmails(String monitorEmails) {
		this.monitorEmails = monitorEmails;
	}

	public Boolean isTemplate() {
		return isTemplate;
	}

	public void setTemplate(Boolean isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	@Override
	public String toString() {
		return "GateLoggerEntity{" +
			"url=" + url +
			", method=" + method +
			", param=" + param +
			", paramRemark=" + paramRemark +
			", requestExam=" + requestExam +
			", responseParam=" + responseParam +
			", errorList=" + errorList +
			", trueExam=" + trueExam +
			", falseExam=" + falseExam +
			", status=" + status +
			", moduleId=" + moduleId +
			", interfaceName=" + interfaceName +
			", remark=" + remark +
			", errors=" + errors +
			", updateBy=" + updateBy +
			", createTime=" + createTime +
			", version=" + version +
			", sequence=" + sequence +
			", header=" + header +
			", fullUrl=" + fullUrl +
			", monitorType=" + monitorType +
			", monitorText=" + monitorText +
			", monitorEmails=" + monitorEmails +
			", isTemplate=" + isTemplate +
			", projectId=" + projectId +
			", contentType=" + contentType +
			"}";
	}
}
