package com.alinesno.cloud.gateway.dashboard.admin.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 每次启动服务器时加载整个表到内存。
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_response")
public class GateResponseEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 方法
     */
	private String method;
    /**
     * 表名，table是SQL关键词不能用
     */
	private String model;
    /**
     * 结构
     */
	private String structure;
    /**
     * 详细说明
     */
	private String detail;
    /**
     * 创建日期
     */
	private Date date;


	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getStructure() {
		return structure;
	}

	public void setStructure(String structure) {
		this.structure = structure;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	@Override
	public String toString() {
		return "GateResponseEntity{" +
			"method=" + method +
			", model=" + model +
			", structure=" + structure +
			", detail=" + detail +
			", date=" + date +
			"}";
	}
}
