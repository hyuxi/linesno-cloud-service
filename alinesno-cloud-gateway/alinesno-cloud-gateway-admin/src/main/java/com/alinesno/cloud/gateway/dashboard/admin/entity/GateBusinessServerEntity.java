package com.alinesno.cloud.gateway.dashboard.admin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.facade.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Entity
@Table(name="gate_business_server")
public class GateBusinessServerEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 业务服务
     */
	private String business;
    /**
     * 银行代码
     */
	private String bank;
    /**
     * 所属接口
     */
	private String api;
    /**
     * 交易码
     */
	private String transaction;
    /**
     * 服务描述
     */
	@Column(name="server_desc")
	private String serverDesc;


	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getServerDesc() {
		return serverDesc;
	}

	public void setServerDesc(String serverDesc) {
		this.serverDesc = serverDesc;
	}


	@Override
	public String toString() {
		return "GateBusinessServerEntity{" +
			"business=" + business +
			", bank=" + bank +
			", api=" + api +
			", transaction=" + transaction +
			", serverDesc=" + serverDesc +
			"}";
	}
}
