package com.alinesno.cloud.gateway.dashboard.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.page.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.LocalMethodBaseController;
import com.alinesno.cloud.gateway.dashboard.admin.entity.GateRequestEntity;
import com.alinesno.cloud.gateway.dashboard.admin.service.IGateRequestService;

/**
 * <p>最好编辑完后删除主键，这样就是只读状态，不能随意更改。需要更改就重新加上主键。

每次启动服务器时加载整个表到内存。
这个表不可省略，model内注解的权限只是客户端能用的，其它可以保证即便服务端代码错误时也不会误删数据。 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-09-22 05:22:27
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("gateway/dashboard/admin/gateRequest")
public class GateRequestController extends LocalMethodBaseController<GateRequestEntity, IGateRequestService> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(GateRequestController.class);

	
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }


}


























