package com.alinesno.cloud.gateway.dashboard;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alinesno.cloud.common.web.basic.auth.enable.EnableHttpBasicAuth;
import com.alinesno.cloud.gateway.dashboard.enable.EnableAlinesnoGateway;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@EnableHttpBasicAuth
@EnableAlinesnoGateway
@SpringBootApplication
public class GateProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(GateProxyApplication.class, args);
	}
	
}