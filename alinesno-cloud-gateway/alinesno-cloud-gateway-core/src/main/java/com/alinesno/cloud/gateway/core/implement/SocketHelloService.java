package com.alinesno.cloud.gateway.core.implement;

import com.alinesno.cloud.gateway.core.annotation.SocketService;
import com.alinesno.cloud.gateway.core.dispather.factory.SocketServiceFactory;
import com.alinesno.cloud.gateway.core.dispather.socket.bean.RequestMessageBean;
import com.alinesno.cloud.gateway.core.dispather.socket.bean.ResponseMessageBean;

import io.netty.channel.ChannelHandlerContext;

/**
 * 业务服务
 * @author LuoAnDong
 * @since 2019年9月21日 下午11:13:51
 */
@SocketService("18090_hellosocket")
public class SocketHelloService implements SocketServiceFactory {

	/**
	 * Socket 服务
	 */
	@Override
	public ResponseMessageBean handle(RequestMessageBean requestMsg, ChannelHandlerContext ctx) {
		return null;
	}

}
