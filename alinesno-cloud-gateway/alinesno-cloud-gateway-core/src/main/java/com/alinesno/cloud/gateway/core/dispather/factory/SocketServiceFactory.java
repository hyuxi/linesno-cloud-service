package com.alinesno.cloud.gateway.core.dispather.factory;

import com.alinesno.cloud.gateway.core.dispather.BaseHandle;
import com.alinesno.cloud.gateway.core.dispather.socket.bean.RequestMessageBean;
import com.alinesno.cloud.gateway.core.dispather.socket.bean.ResponseMessageBean;

import io.netty.channel.ChannelHandlerContext;

/**
 * HTTP请求分发器 
 * @author LuoAnDong
 * @sine 2018年8月5日 下午12:48:27
 */
public interface SocketServiceFactory extends  BaseHandle{

	/**
	 * Socket 请求处理方法
	 * @param ctx 
	 * @param requestMsg 
	 * @return
	 */
	ResponseMessageBean handle(RequestMessageBean requestMsg, ChannelHandlerContext ctx) ; 
	
}