package com.alinesno.cloud.gateway.core.exception;

/**
 * 多数情况下，创建自定义异常需要继承Exception，本例继承Exception的子类RuntimeException,放在javax或者java目录下面,dubbo不做拦截
 * 
 * @author LuoAnDong
 * @since 2019年9月17日 下午9:18:23
 */
@SuppressWarnings("serial")
public class GatewayException extends RuntimeException {

	private String code; // 异常对应的返回码
	private String message; // 异常对应的描述信息
	private String threadInfo;

	public GatewayException() {
		super();
	}

	protected GatewayException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GatewayException(String message) {
		super(message);
		this.message = message;
	}

	public GatewayException(String message, String threadInfo, Throwable cause) {
		super(message, cause);
		this.threadInfo = threadInfo;
	}

	public GatewayException(Throwable cause) {
		super(cause);
	}

	public GatewayException(String retCd, String msgDes) {
		super();
		this.code = retCd;
		this.message = msgDes;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getThreadInfo() {
		return threadInfo;
	}

	public void setThreadInfo(String threadInfo) {
		this.threadInfo = threadInfo;
	}
}
