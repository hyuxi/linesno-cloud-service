package com.alinesno.cloud.gateway.core.filter.rule;

import java.net.InetSocketAddress;

import org.springframework.stereotype.Service;

import com.alinesno.cloud.gateway.core.dispather.socket.bean.RequestMessageBean;
import com.alinesno.cloud.gateway.core.filter.ExecutionChain;
import com.alinesno.cloud.gateway.core.filter.FilterBean;

/**
 * 过滤器
 * @author LuoAnDong
 * @since 2019年9月22日 上午12:15:30
 */
@Service
public class RuleExecutionChain extends ExecutionChain{

	/**
	 * 过滤器调用 
	 * @param insocket
	 * @param requestMsg
	 * @return
	 */
	public FilterBean doFilter(InetSocketAddress insocket, RequestMessageBean requestMsg) {
		FilterBean bean = new FilterBean() ; 
		
		return bean ;
	}

}
