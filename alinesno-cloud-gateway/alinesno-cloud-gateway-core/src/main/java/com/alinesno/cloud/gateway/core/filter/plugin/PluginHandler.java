package com.alinesno.cloud.gateway.core.filter.plugin;

import com.alinesno.cloud.gateway.core.filter.Filter;

/**
 * 规则引擎基础
 * @author LuoAnDong
 * @since 2019年9月21日 下午11:32:08
 */
public interface PluginHandler  extends Filter {

}
