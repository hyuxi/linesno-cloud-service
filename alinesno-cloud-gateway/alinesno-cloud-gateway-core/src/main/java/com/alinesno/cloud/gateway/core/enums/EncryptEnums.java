package com.alinesno.cloud.gateway.core.enums;

/**
 * 加密方式
 * @author LuoAnDong
 * @since 2019年9月22日 上午2:52:54
 */
public enum EncryptEnums {

	MD5 , 
	ASC 
}
