package com.alinesno.cloud.gateway.core.filter.handler;

import java.net.InetSocketAddress;
import java.util.Map;

import com.alinesno.cloud.gateway.core.annotation.GatewayFilter;
import com.alinesno.cloud.gateway.core.exception.GatewayException;
import com.alinesno.cloud.gateway.core.filter.FilterHandler;

/**
 * 数据监控过滤 
 * @author LuoAnDong
 * @since 2019年9月21日 下午11:26:56
 */
@GatewayFilter
public class MonitorFilter implements FilterHandler{

	@Override
	public void doFilter(InetSocketAddress insocket, Map<String, Object> params) throws GatewayException  {
	}

}
