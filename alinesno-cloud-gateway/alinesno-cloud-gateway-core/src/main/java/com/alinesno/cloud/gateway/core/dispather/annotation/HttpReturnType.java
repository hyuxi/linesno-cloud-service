package com.alinesno.cloud.gateway.core.dispather.annotation;
/**
 * 接口数据请求的类型
 * @author LuoAnDong
 * @date 2017年12月1日 下午9:01:56
 */
public enum HttpReturnType {
	
	JSON("json") , 
	XML("xml") ; 
	
	private String value ; 
	
	HttpReturnType(String value) {
		this.value = value ; 
	}
	
	String value() {
		return value ; 
	}
}